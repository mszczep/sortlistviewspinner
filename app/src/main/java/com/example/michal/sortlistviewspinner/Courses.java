package com.example.michal.sortlistviewspinner;

/**
 * Created by Michal on 2017-11-07.
 */
public class Courses {
    private long id;
    private long langID;
    private String courseName;
    private String courseLevel;
    private String courseTime;

    public long getId() {
        return id;
    }

    public void setID(long id) {
        this.id = id;
    }
    public void setCourseLangID(long langID)
    {
        this.langID = langID;
    }
    public void setCourseLevel(String courseLevel) {
        this.courseLevel = courseLevel;
    }
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
    public void setCourseTime(String courseTime) {
        this.courseTime = courseTime;
    }

    public long getLangID() {
        return langID;
    }

    public String getCourseName() {
        return courseName;
    }
    public String getCourseLevel() {
        return courseLevel;
    }
    public String getCourseTime() {
        return courseTime;
    }


}
