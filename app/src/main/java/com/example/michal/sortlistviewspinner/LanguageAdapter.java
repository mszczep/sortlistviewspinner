package com.example.michal.sortlistviewspinner;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by Michal on 2017-11-07.
 */

public class LanguageAdapter extends ArrayAdapter<Language> {

    private Context context;
    private List<Language> languageList;

    public LanguageAdapter(Context context, int textViewResourceId,
                           List<Language> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.languageList = values;
    }

    public int getCount(){
        return languageList.size();
    }

    public Language getItem(int position){
        return languageList.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = new TextView(context);
        view.setTextColor(Color.BLACK);
        view.setGravity(Gravity.CENTER);
        view.setText(languageList.get(position).getLangName());

        return view;
    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView view = new TextView(context);
        view.setTextColor(Color.BLACK);
        view.setText(languageList.get(position).getLangName());
        view.setHeight(60);

        return view;
    }
}
