package com.example.michal.sortlistviewspinner;

/**
 * Created by Michal on 2017-11-07.
 */

public class Language {
    private long langID;
    private String langName;

    public long getLangID() {
        return langID;
    }

    public void setLangID(long langID) {
        this.langID = langID;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }
}

