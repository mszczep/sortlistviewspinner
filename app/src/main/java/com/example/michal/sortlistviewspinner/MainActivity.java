package com.example.michal.sortlistviewspinner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    private List<Courses> coursesList = new ArrayList<Courses>();
    private List<Language> languageList = new ArrayList<Language>();
    private ListView listView;
    private CoursesAdapter adapter;
    private LanguageAdapter languageAdapter;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner)findViewById(R.id.spinnerLanguage);
        listView = (ListView) findViewById(R.id.listViewLanguage);
        languageAdapter = new LanguageAdapter(this,android.R.layout.simple_spinner_dropdown_item,loadLanguages());
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(languageAdapter);
        spinner.setOnItemSelectedListener(this);
        loadCourses();
        adapter = new CoursesAdapter(this, coursesList);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<Language> loadLanguages(){
        languageList = new ArrayList<Language>();


        Language lang1 = new Language();
        lang1.setLangID(1);
        lang1.setLangName("English");
        languageList.add(lang1);

        Language lang2 = new Language();
        lang2.setLangID(2);
        lang2.setLangName("German");
        languageList.add(lang2);

        Language lang3 = new Language();
        lang3.setLangID(3);
        lang3.setLangName("French");
        languageList.add(lang3);

        Language lang4 = new Language();
        lang4.setLangID(4);
        lang4.setLangName("Polish");
        languageList.add(lang4);



        return languageList;
    }

    private List<Courses> loadCourses(){

        coursesList = new ArrayList<Courses>();


        Courses course1 = new Courses();
        course1.setID(1);
        course1.setCourseLangID(1);
        course1.setCourseName("Verbs");
        course1.setCourseLevel("A2");
        course1.setCourseTime("5");
        coursesList.add(course1);

        Courses course2 = new Courses();
        course2.setID(2);
        course2.setCourseLangID(2);
        course2.setCourseName("Verben");
        course2.setCourseLevel("A2");
        course2.setCourseTime("5");
        coursesList.add(course2);

        Courses course3 = new Courses();
        course3.setID(3);
        course3.setCourseLangID(3);
        course3.setCourseName("Verbes");
        course3.setCourseLevel("A2");
        course3.setCourseTime("5");
        coursesList.add(course3);


        Courses course4 = new Courses();
        course4.setID(4);
        course4.setCourseLangID(4);
        course4.setCourseName("Czasowniki");
        course4.setCourseLevel("A2");
        course4.setCourseTime("5");
        coursesList.add(course4);

        Courses course5 = new Courses();
        course5.setID(5);
        course5.setCourseLangID(1);
        course5.setCourseName("Nouns");
        course5.setCourseLevel("B2");
        course5.setCourseTime("8");
        coursesList.add(course5);

        Courses course6 = new Courses();
        course6.setID(6);
        course6.setCourseLangID(1);
        course6.setCourseName("More nouns");
        course6.setCourseLevel("C2");
        course6.setCourseTime("12");
        coursesList.add(course6);


         Courses course7 = new Courses();
        course7.setID(7);
        course7.setCourseLangID(2);
        course7.setCourseName("Substantive");
        course7.setCourseLevel("A1");
        course7.setCourseTime("6");
        coursesList.add(course7);



        return coursesList;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Language language = languageAdapter.getItem(position);


        adapter.getFilter().filter(Long.toString(language.getLangID()),new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {

            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}