package com.example.michal.sortlistviewspinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2017-11-07.
 */

public class CoursesAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater mLayoutInflater;
    private List<Courses> coursesList;
    private List<Courses> coursesFilterList;
    private AddressFilter addressFilter;
    private Context context;

    public CoursesAdapter(Context context, List data){
        coursesList = data;
        coursesFilterList =data;
        mLayoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return coursesList.size();
    }

    @Override
    public Courses getItem(int position) {
        return coursesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View updateView;
        ViewHolder viewHolder;
        if (view == null) {
            updateView = mLayoutInflater.inflate(R.layout.course_single_item, null);
            viewHolder = new ViewHolder();

            viewHolder.tvName = (TextView) updateView.findViewById(R.id.courseName);
            viewHolder.tvArea = (TextView) updateView.findViewById(R.id.courseLevel);
            viewHolder.tvStreet = (TextView) updateView.findViewById(R.id.courseTime);

            updateView.setTag(viewHolder);

        } else {
            updateView = view;
            viewHolder = (ViewHolder) updateView.getTag();
        }

        final Courses item = getItem(position);

        viewHolder.tvName.setText(item.getCourseName());
        viewHolder.tvArea.setText(item.getCourseTime());
        viewHolder.tvStreet.setText(String.valueOf(item.getCourseLevel()));

        return updateView;
    }

    @Override
    public Filter getFilter() {
        if (addressFilter == null) {
            addressFilter = new AddressFilter();
        }
        return addressFilter;
    }

    static class ViewHolder{
        TextView tvName;
        TextView tvArea;
        TextView tvStreet;
    }


    private class AddressFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            long langId= Long.parseLong(constraint.toString());
            FilterResults results = new FilterResults();

            if (langId > 0) {
                ArrayList<Courses> filterList = new ArrayList<Courses>();
                for (int i = 0; i < coursesFilterList.size(); i++) {

                    if ( (coursesFilterList.get(i).getLangID() )== langId) {

                        Courses courses = coursesFilterList.get(i);
                        filterList.add(courses);
                    }
                }

                results.count = filterList.size();
                results.values = filterList;

            } else {

                results.count = coursesFilterList.size();
                results.values = coursesFilterList;

            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            coursesList = (ArrayList<Courses>)results.values;
            notifyDataSetChanged();
        }
    }
}
